FROM node:10

ARG USER_UID=4732
ARG USER_GID=987
ARG SECRET_WORD

ENV SECRET_WORD=$SECRET_WORD

COPY . /quest
WORKDIR /quest

RUN mkdir /.npm \ 
    && chown -R $USER_UID:$USER_GID /.npm /quest

USER $USER_UID
RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
