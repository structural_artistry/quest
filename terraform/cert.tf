resource "tls_private_key" "quest" {
  algorithm = "RSA"
}
resource "tls_self_signed_cert" "quest" {
  key_algorithm   = tls_private_key.quest.algorithm
  private_key_pem = tls_private_key.quest.private_key_pem

  subject {
    common_name  = "nobody.com"
    organization = "nobody"
  }

  validity_period_hours = 720

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}
resource "aws_acm_certificate" "quest" {
  private_key      = tls_private_key.quest.private_key_pem
  certificate_body = tls_self_signed_cert.quest.cert_pem
}
