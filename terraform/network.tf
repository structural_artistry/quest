resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "main"
  }
}

resource "aws_default_route_table" "rt" {
  default_route_table_id = aws_vpc.main.default_route_table_id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.public_subnet.id
  }

  tags = {
    Name = "default"
  }
}

resource "aws_subnet" "public" {
  count                   = length(var.availability_zones)
  cidr_block              = element(var.public_subnet_cidr_blocks_az_ordered, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  vpc_id                  = aws_vpc.main.id
  map_public_ip_on_launch = true

  tags = {
    Name = "public_${element(var.availability_zones, count.index)}"
  }
}

resource "aws_subnet" "private" {
  count                   = length(var.availability_zones)
  cidr_block              = element(var.private_subnet_cidr_blocks_az_ordered, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  vpc_id                  = aws_vpc.main.id
  tags = {
    Name = "private_${element(var.availability_zones, count.index)}"
  }
}

resource "aws_internet_gateway" "public_subnet" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "public_subnet"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.main.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.public_subnet.id
}

resource "aws_eip" "nat_gw" {
  count      = length(var.availability_zones)
  vpc        = true
  depends_on = [aws_internet_gateway.public_subnet]

  tags = {
    Name = "nat_gw_${count.index}"
  }
}
resource "aws_nat_gateway" "public_subnet" {
  count      = length(var.availability_zones)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  allocation_id = element(aws_eip.nat_gw.*.id, count.index)

  tags = {
    Name = "public_subnet_${element(aws_subnet.public.*.availability_zone, count.index)}"
  }
}

resource "aws_route_table" "private_subnet" {
  count      = length(var.availability_zones)
  vpc_id     = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.public_subnet.*.id, count.index)
  }

  tags = {
    Name = "private_subnet_${element(var.availability_zones, count.index)}"
  }
}

resource "aws_route_table_association" "private" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private_subnet.*.id, count.index)
}
