variable "aws_region" {
  default = "us-west-2"
}
variable "vpc_cidr_block" {
  default = "172.30.0.0/26"
}
variable "availability_zones" {
  default = ["us-west-2a", "us-west-2b"]
}
variable "public_subnet_cidr_blocks_az_ordered" {
  default = ["172.30.0.0/28", "172.30.0.16/28"]
}
variable "private_subnet_cidr_blocks_az_ordered" {
  default = ["172.30.0.32/28", "172.30.0.48/28"]
}
variable "web_task_port" {
  default = 3000 
}
variable "web_task_count" {
  default = 2
}
variable "web_task_cpu" {
  default = 256
}
variable "web_task_memory" {
  default = 512
}
