[
  {
    "name": "web",
    "image": "public.ecr.aws/r3t4h8i5/alexi:quest_latest",
    "cpu": ${web_task_cpu},
    "memory": ${web_task_memory},
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${web_task_port},
        "hostPort": ${web_task_port}
      }
    ]
  }
]
