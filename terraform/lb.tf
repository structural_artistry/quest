resource "aws_alb" "web" {
  name            = "web"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.lb_web.id]

  tags = {
    Name = "web"
  }
}

resource "aws_alb_target_group" "web" {
  name        = "web"

  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

 health_check {
   healthy_threshold   = "3"
   interval            = "30"
   protocol            = "HTTP"
   matcher             = "200"
   timeout             = "3"
   path                = "/loadbalanced"
   unhealthy_threshold = "2"
  }

  tags = {
    Name = "web"
  }
}

resource "aws_alb_listener" "web_https" {
  load_balancer_arn = aws_alb.web.arn
  port              = 443
  protocol          = "HTTPS"

  certificate_arn   = aws_acm_certificate.quest.arn

  default_action {
    target_group_arn = aws_alb_target_group.web.id
    type             = "forward"
  }
}

resource "aws_lb_listener" "web_http" {
  load_balancer_arn = aws_alb.web.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

