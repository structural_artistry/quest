# WEB
resource "aws_ecs_cluster" "web" {
  name = "web"

  tags = {
    Name = "web"
  }
}
data "template_file" "web" {
  template = file("web.json.tpl")

  vars = {
    web_task_port   = var.web_task_port
    web_task_cpu    = var.web_task_cpu
    web_task_memory = var.web_task_memory
  }
}
resource "aws_ecs_task_definition" "web" {
  family                   = "web"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.web_task_cpu
  memory                   = var.web_task_memory
  container_definitions    = data.template_file.web.rendered

  tags = {
    Name = "web"
  }
}
resource "aws_ecs_service" "web" {
  name            = "web"
  cluster         = aws_ecs_cluster.web.id
  task_definition = aws_ecs_task_definition.web.arn
  desired_count   = var.web_task_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.web_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.web.id
    container_name   = "web"
    container_port   = var.web_task_port
  }

  depends_on = [aws_alb_listener.web_https, aws_iam_role_policy_attachment.execution_role]

  tags = {
    Name = "web"
  }
}
