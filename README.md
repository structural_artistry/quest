# QUEST SUBMIT NOTES

### Observations
* Docker image is rather large, maybe could go to alpine or smaller for this app
* It's interesting that deployed on Fargate the app does not detect that it is running on Docker. I wonder what that's about and that perhaps could be due to not running on root user and app maybe not able to access what it wants. Or something inherent to how it deploys on Fargate.

### Completed Items and Notes
##### 1) AWS/Secret page: PDF in project root 'step_1_secret_page_image.png' 
##### 2) Docker: Dockerfile in project root
* Run as non-root user
* Build image with secret word: `docker image build --build-arg SECRET_WORD=TwelveFactor -t  quest:secret_word .`
* Note that if the secret word is really a secret, would be better to load it on the container via an env file or secret from AWS secret manager during the spinup on ECS, as the secret built into the image is discoverable by anyone who can obtain the image (esp in this case as it's on a public registry). 
##### 3) SECRET_WORD env variable: 'TwelveFactor', built into image at public.ecr.aws/r3t4h8i5/alexi:quest_latest that is used on the deploy below via terraform 
##### 4) Loadbalancer: implemented ALB, redirects to TLS and terminates TLS, also on the deploy below
##### 5) Terraform and/or Cloudformation: 
* Built out VPC with necessary objects to not use default VPC and use non-TF managed 'assumed' objects.
* Debated to use EKS for deploy vs Fargate, but for one-click, would have to add ancillary comments for kubectl or use something like ansible to run them from TF. So went with Fargate.
* Added self signed cert re TF resources, nice to know about this :)
* Running two instances of the app, as what fun is a single instance behind a LB (though I do realise AWS LB's provide security features even for a single container)
* App (Fargate/Docker) runs on private subnet, only public accessible is LB, as necessary added NAT so app can get out (well, and even be able to load the image when starting). Only ingreess on the app port for SG for container.
##### 6) TLS: see above, evident on the deploy, try going in on http...

### Deploy URL 
##### web-362227299.us-west-2.elb.amazonaws.com 

